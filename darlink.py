"""
Pure-Python implementation of Varlink protocol <https://varlink.org/>.

This Python binding supports hooking into event loops via Python’s standard
asyncio module.
"""
#+
# Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import io
import re
import enum
from weakref import \
    ref as weak_ref
import json
import time
import struct
import socket
import select
import asyncio
import atexit
import inspect
import ast
import arpeggio
  # docs: <https://textx.github.io/Arpeggio/2.0/>
import arpeggio.peg
arp = arpeggio
del arpeggio

#+
# Macro preprocessor
#
# This mechanism allows for synchronous and asynchronous versions of
# classes to share common code. Methods that come in both forms will
# look something like
#
#     async def «method» ...
#         ...
#         if ASYNC :
#             «asynchronous variant»
#         else :
#             «synchronous variant»
#         #end if
#         ...
#     #end «method»
#
# where “ASYNC” is an undefined variable, used as a special condition
# marker at every point where the two variants diverge.
#
# The abstract syntax tree for this code will be processed into two
# variants, that (if they were decompiled to source form) would look
# like
#
#     def «method» ...
#         ...
#         «synchronous variant»
#         ...
#     #end «method»
#
#     async def «method» ...
#         ...
#         «asynchronous variant»
#         ...
#     #end «method»
#
# Notice that all references to “ASYNC” disappear, leaving the
# synchronous version executing unconditionally synchronous code,
# and the asynchronous version unconditionally using the asynchronous
# variants of that code.
#
# However, note that __init__ is not allowed to be async def
# (must return None, not a coroutine), so if you want to do
# asynchronous object creation, you need to do object construction
# in a __new__ method instead.
#-

class ConditionalExpander(ast.NodeTransformer) :
    "generates synchronous or asynchronous variants of a class from common code."

    def __init__(self, classname, newclassname, is_async) :
        self.classname = classname
        self.newclassname = newclassname
        self.is_async = is_async
    #end __init__

    def visit_ClassDef(self, node) :
        body = list(self.visit(b) for b in node.body)
        result = node
        if result.name == self.classname :
            # any inner classes keep the same name in both versions
            result.name = self.newclassname
        #end if
        result.body = body
        return \
            result
    #end visit_ClassDef

    def visit_AsyncFunctionDef(self, node) :
        body = list(self.visit(b) for b in node.body)
        if self.is_async :
            result = node
            result.body = body
        else :
            result = ast.FunctionDef \
              (
                name = node.name,
                args = node.args,
                body = body,
                decorator_list = node.decorator_list,
                returns = node.returns
              )
            if hasattr(node, "type_comment") :
                result.type_comment = node.type_comment
            #end if
        #end if
        return \
            result
    #end visit_AsyncFunctionDef

    def visit_If(self, node) :
        result = None
        if isinstance(node.test, ast.Name) :
            if node.test.id == "ASYNC" and isinstance(node.test.ctx, ast.Load) :
                if self.is_async :
                    if len(node.body) > 1 :
                        result = ast.If \
                          (
                            test = ast.Constant(True),
                            body = node.body,
                            orelse = []
                          )
                    elif len(node.body) == 1 :
                        result = node.body[0]
                    else :
                        result = ast.Pass()
                    #end if
                else :
                    if len(node.orelse) > 1 :
                        result = ast.If \
                          (
                            test = ast.Constant(True),
                            body = node.orelse,
                            orelse = []
                          )
                    elif len(node.orelse) == 1 :
                        result = node.orelse[0]
                    else :
                        result = ast.Pass()
                    #end if
                #end if
            #end if
        #end if
        if result == None :
            result = ast.If \
              (
                test = node.test,
                body = list(self.visit(b) for b in node.body),
                orelse = list(self.visit(b) for b in node.orelse),
              )
        #end if
        return \
            result
    #end visit_If

#end ConditionalExpander

def def_sync_async_classes(class_template, sync_classname, async_classname) :
    "takes the class object class_template and generates the synchronous and" \
    " asynchronous forms of the class, defining them in this module’s global" \
    " namespace where the former is named sync_classname and the latter is named" \
    " async_classname."
    src = inspect.getsource(class_template)
    # need two copies of the AST, since the expansion process modifies it in-place
    syntax = ast.parse(src, mode = "exec")
    sync_version = ConditionalExpander \
      (
        classname = class_template.__name__,
        newclassname = sync_classname,
        is_async = False
      ).visit(syntax)
    syntax = ast.parse(src, mode = "exec")
    async_version = ConditionalExpander \
      (
        classname = class_template.__name__,
        newclassname = async_classname,
        is_async = True
      ).visit(syntax)
    ast.fix_missing_locations(sync_version)
    ast.fix_missing_locations(async_version)
    exec(compile(sync_version, filename = __file__, mode = "exec"), globals())
    exec(compile(async_version, filename = __file__, mode = "exec"), globals())
#end def_sync_async_classes

#+
# Useful stuff
#-

def await_any(awaiting) :
    "convenient wrapper around asyncio.wait for a common case."
    return \
        asyncio.wait(awaiting, return_when = asyncio.FIRST_COMPLETED)
#end await_any

class SOCK_NEED(enum.Enum) :
    "need to wait for socket to allow I/O of this type before further" \
    " communication can proceed."
    NOTHING = 0 # I/O can proceed
    READABLE = 1 # wait for socket to become readable
    WRITABLE = 2 # wait for socket to become writable
#end SOCK_NEED

IOBUFSIZE = 4096 # size of most I/O buffers

def sock_wait(sock, recv, send, timeout = None) :
    "waits until the specified socket has something to be received or sent," \
    " or the timeout (if specified) elapses."
    if not (recv or send) :
        raise RuntimeError("need to wait for either receive or send or both")
    #end if
    poll = select.poll()
    poll.register \
      (
        sock,
        (0, select.POLLOUT)[send] | (0, select.POLLIN)[recv]
      )
    ready = poll.poll \
      (
        (lambda : None, lambda : round(timeout * 1000))
            [timeout != None]()
      )
    if len(ready) != 0 :
        ready = ready[0]
        assert ready[0] == sock.fileno()
        flags = ready[1]
        receiving = flags & select.POLLIN != 0
        sending = flags & select.POLLOUT != 0
    else :
        receiving = sending = False
    #end if
    return \
        (receiving, sending)
#end sock_wait

async def sock_wait_async(sock, recv, send, timeout = None) :
    "waits until the specified socket has something to be received or sent," \
    " or the timeout (if specified) elapses."
    if not (recv or send) :
        raise RuntimeError("need to wait for either receive or send or both")
    #end if
    loop = asyncio.get_running_loop()
    ready = loop.create_future()
    flags = 0

    def fd_ready(writing) :
        nonlocal flags
        flags |= (select.POLLIN, select.POLLOUT)[writing]
        if not ready.done() :
            ready.set_result(False)
        #end if
    #end fd_ready

    def fd_timeout() :
        if not ready.done() :
            ready.set_result(True)
        #end if
    #end fd_timeout

    timeout_task = None
    if recv :
        loop.add_reader(sock, fd_ready, False)
    #end if
    if send :
        loop.add_writer(sock, fd_ready, True)
    #end if
    if timeout != None :
        timeout_task = loop.call_later(timeout, fd_timeout)
    #end if
    timed_out = await ready
    if recv :
        loop.remove_reader(sock)
    #end if
    if send :
        loop.remove_writer(sock)
    #end if
    if timeout_task != None :
        timeout_task.cancel()
    #end if
    if timed_out :
        receiving = sending = False
    else :
        receiving = select.POLLIN & flags != 0
        sending = select.POLLOUT & flags != 0
    #end if
    return \
        (receiving, sending)
#end sock_wait_async

class AbsoluteTimeout :
    "given a relative timeout in seconds from the time of" \
    " instantiation, produces any number of successive relative" \
    " timeouts that always end at the same absolute time. This way," \
    " the timeout applies to a complete sequence of operations being" \
    " performed. If the given timeout is None, then returned relative" \
    " timeouts are also None."

    def __init__(self, timeout) :
        if timeout != None :
            self.deadline = time.monotonic() + timeout
        else :
            self.deadline = None
        #end if
    #end __init__

    @property
    def timeout(self) :
        if self.deadline != None :
            result = max(self.deadline - time.monotonic(), 0)
        else :
            result = None
        #end if
        return \
            result
    #end timeout

#end AbsoluteTimeout

class DanglingRefException(Exception) :

    def __init__(self, msg) :
        super().__init__((msg,))
    #end __init__

#end DanglingRefException

def _wderef(w_self, parent) :
    self = w_self()
    if self == None :
        raise DanglingRefException("%s has gone away" % parent)
    #end if
    return self
#end _wderef

#+
# Socket wrapper class
#-

SOMAXCONN = None # to begin with

def get_SOMAXCONN() :
    "returns the Linux kernel-configured value of SOMAXCONN."
    global SOMAXCONN
    if SOMAXCONN == None :
        SOMAXCONN = int(open("/proc/sys/net/core/somaxconn", "rt").read().strip())
    #end if
    return \
        SOMAXCONN
#end get_SOMAXCONN

class SocketWrapper :
    "a wrapper around unencrypted socket connections, providing sync or async" \
    " transfers with optional timeouts."

    def __init__(self, sock = None) :
        if sock == None :
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #end if
        self.sock = sock
        self.sock_need = SOCK_NEED.NOTHING
          # set by immediate-mode calls, used by I/O-polling hooks to decide what to wait for
    #end __init__

    async def connect(self, addr, timeout = None) :
        self.sock.setblocking(False)
        try :
            self.sock.connect(addr)
        except BlockingIOError as err :
            if err.errno != errno.EINPROGRESS :
                raise
            #end if
            done_connect = False
        else :
            done_connect = True # would never actually occur?
        #end try
        if not done_connect :
            if ASYNC :
                recv, send = await sock_wait_async(self.sock, False, True, timeout)
            else :
                recv, send = sock_wait(self.sock, False, True, timeout)
            #end if
            if not send :
                self.sock.close()
                raise TimeoutError("socket taking too long to connect")
            #end if
            connerr = self.sock.getsockopt(socket.SOL_SOCKET, socket.SO_ERROR)
            if connerr != 0 :
                raise OSError(connerr, "socket connection failure: %s" % os.strerror(connerr))
            #end if
        #end if
    #end connect

    async def recv(self, nrbytes, timeout = None) :
        self.sock_need = SOCK_NEED.NOTHING
        deadline = AbsoluteTimeout(timeout)
        while True :
            try :
                got = self.sock.recv(nrbytes, socket.MSG_DONTWAIT)
            except BlockingIOError :
                got = None
            #end try
            if got != None :
                break
            if deadline.timeout == 0 :
                break
            if ASYNC :
                recv = (await sock_wait_async(self.sock, True, False, deadline.timeout))[0]
            else :
                recv = sock_wait(self.sock, True, False, deadline.timeout)[0]
            #end if
            if not recv :
                break
        #end while
        return \
            got
    #end recv

    def recvimmed(self, nrbytes) :
        self.sock_need = SOCK_NEED.NOTHING
        try :
            data = self.sock.recv(nrbytes, socket.MSG_DONTWAIT)
        except BlockingIOError :
            data = None
            self.sock_need = SOCK_NEED.READABLE
        #end try
        return \
            data
    #end recvimmed

    async def sendall(self, data, timeout = None) :
        self.sock_need = SOCK_NEED.NOTHING
        deadline = AbsoluteTimeout(timeout)
        while True :
            try :
                sent = self.sock.send(data, socket.MSG_DONTWAIT)
            except BlockingIOError :
                sent = 0
            #end try
            data = data[sent:]
            if len(data) == 0 :
                timed_out = False
                break
            #end if
            if ASYNC :
                send = (await sock_wait_async(self.sock, False, True, deadline.timeout))[1]
            else :
                send = sock_wait(self.sock, False, True, deadline.timeout)[1]
            #end if
            if not send :
                timed_out = True
                break
            #end if
        #end while
        if timed_out :
            raise TimeoutError("socket taking too long to send")
        #end if
    #end sendall

    def sendimmed(self, data) :
        self.sock_need = SOCK_NEED.NOTHING
        try :
            sent = self.sock.send(data, socket.MSG_DONTWAIT)
        except BlockingIOError :
            sent = 0
            self.sock_need = SOCK_NEED.WRITABLE
        #end try
        return \
            sent
    #end sendimmed

    def close(self) :
        if self.sock != None :
            self.sock.close() # is there anything blocking about this?
            self.sock = None
        #end if
    #end close

    def fileno(self) :
        return \
            self.sock.fileno()
    #end fileno

    def poll_register(self, poll) :
        "registers this socket with the given poll object, if it needs to." \
        " Returns True iff registration was done."
        register = self.sock_need != SOCK_NEED.NOTHING
        if register :
            poll.register \
              (
                self.sock,
                {
                    SOCK_NEED.READABLE : select.POLLIN,
                    SOCK_NEED.WRITABLE : select.POLLOUT,
                }[self.sock_need]
              )
        #end if
        return \
            register
    #end poll_register

    async def io_wait(self, timeout = None) :
        "blocks as appropriate until the socket is ready to try immediate-mode" \
        " reading or writing, as determined by the state saved from the last" \
        " recvimmed/sendimmed call."
        if self.sock_need != SOCK_NEED.NOTHING :
            if ASYNC :
                recv, send = await sock_wait_async \
                  (
                    self.sock,
                    recv = self.sock_need == SOCK_NEED.READABLE,
                    send = self.sock_need == SOCK_NEED.WRITABLE,
                    timeout = timeout
                  )
            else :
                poll = select.poll()
                assert self.poll_register(poll)
                ready = poll.poll(timeout)
                if len(ready) > 0 :
                    assert len(ready) == 1
                    ready, = ready
                    assert ready[0] == self.sock.fileno()
                    flags = ready[1]
                    recv = flags & select.POLLIN != 0
                    send = flags & select.POLLOUT != 0
                else :
                    recv = send = False
                #end if
            #end if
            self.sock_need = SOCK_NEED.NOTHING
        else :
            recv = send = True
        #end if
        return \
            recv, send
    #end io_wait

#end SocketWrapper

def_sync_async_classes(SocketWrapper, "SocketWrapper", "SocketWrapperAsync")

#+
# Varlink connections
#-

def parse_url(url) :
    if not url.startswith("unix:") :
        raise ValueError("only unix:* URLs supported for now: %s" % url)
    #end if
    path = url[5:]
    if path.startswith("@") :
        path = b"\x00" + path[1:].encode()
    #end if
    return \
        (socket.AF_UNIX, path)
#end parse_url

class Connection :
    "represents a Varlink connection for responding to requests from clients," \
    " or for making requests to a specified server."

    __slots__ = \
        (
            "__weakref__",
            "sock", "accepting", "peer",
            "rcvbuf", "eof",
            "_send_queue", "_send_queue_task", "_send_queue_empty",
            "receiver_task",
        )

    class Peer :
        "some simple description of the process at the other end" \
        " of a unix-family socket."

        __slots__ = ("pid", "uid", "gid")

        def __init__(self, sock) :
            # peeraddr on accept() is always empty for unix-family socket
            # connection. Even sock.getpeername() returns empty string.
            u_cred = "=iII"
              # as per sys/socket.h: struct { pid_t pid; uid_t uid; gid_t gid; }
              # -- all 32 bits on Linux AMD64
            peercred = sock.getsockopt \
              (
                socket.SOL_SOCKET,
                socket.SO_PEERCRED,
                struct.calcsize(u_cred)
              )
            self.pid, self.uid, self.gid = struct.unpack(u_cred, peercred)
        #end __init__

        def __str__(self) :
            return \
                (
                    "%s(pid=%d, uid=%d, gid=%d)"
                %
                    (type(self).__name__, self.pid, self.uid, self.gid)
                )
        #end __str__

    #end Peer

    async def __new__(celf, *, bind = None, connect = None, sock = None, timeout = None) :
        if sum(int(a != None) for a in (bind, connect, sock)) != 1 :
            raise ValueError("specify exactly one of bind, connect or sock args")
        #end if
        self = super().__new__(celf)
        self.accepting = bind != None
        self.peer = None
        self.eof = False
        if self.accepting :
            self._send_queue = None
        else :
            self._send_queue = []
        #end if
        self._send_queue_task = None
        self._send_queue_empty = None
        self.receiver_task = None
        if ASYNC :
            SockWrap = SocketWrapperAsync
        else :
            SockWrap = SocketWrapper
        #end if
        if bind != None :
            fam, path = parse_url(bind)
            sock = socket.socket(fam, socket.SOCK_STREAM)
            self.sock = SockWrap(sock)
            self.sock.sock.bind(path)
            self.sock.sock.listen(get_SOMAXCONN())
        elif connect != None :
            fam, path = parse_url(connect)
            sock = socket.socket(fam, socket.SOCK_STREAM)
            self.sock = SockWrap(sock)
            if ASYNC :
                await self.sock.connect(path, timeout)
            else :
                self.sock.connect(path, timeout)
            #end if
        elif sock != None :
            self.sock = SockWrap(sock)
        #end if
        self.rcvbuf = b""
        return \
            self
    #end __new__

    @property
    def is_open(self) :
        return \
            self.sock != None
    #end is_open

    def close(self) :
        if self.sock != None :
            if self._send_queue_task != None :
                self._send_queue_task.cancel()
                self._send_queue_task = None
            #end if
            if self.receiver_task != None :
                self.receiver_task.cancel()
                self.receiver_task = None
            #end if
            self.sock.close()
            self.sock = None
        #end if
    #end close
    __del__ = close

    async def accept(self, timeout = None) :
        "accepts another incoming connection on a server-side socket."
        assert self.accepting, "this is not a server-side socket"
        deadline = AbsoluteTimeout(timeout)
        self_sock = self.sock
        Connection = type(self)
        w_self = weak_ref(self)
        self = None # avoid circular refs, not needed any more
        while True :
            if not _wderef(w_self, "Connection").is_open :
                client_sock = None
                break
            #end if
            self_sock.sock_need = SOCK_NEED.READABLE # indicates incoming connection
            if ASYNC :
                recv = (await self_sock.io_wait(deadline.timeout))[0]
            else :
                recv = self_sock.io_wait(deadline.timeout)[0]
            #end if
            if recv :
                try :
                    client_sock, peeraddr = self_sock.sock.accept()
                except ConnectionAbortedError :
                    pass # retry until timeout expires
                else :
                    break
                #end if
            else :
                # timed out
                client_sock = None
                break
            #end if
        #end while
        if _wderef(w_self, "Connection").is_open :
            if client_sock == None :
                raise TimeoutError("taking too long to receive next connection attempt")
            #end if
            client_sock.setblocking(False)
            if ASYNC :
                conn = await Connection(sock = client_sock)
            else :
                conn = Connection(sock = client_sock)
            #end if
            conn.peer = Connection.Peer(client_sock)
        else :
            conn = None
        #end if
        return \
            conn
    #end accept

    async def _send(self, msg, timeout = None) :
        # common internal routine for both send() and queue_send(), assuming
        # higher-level wrappers will coordinate ordering of outgoing messages.
        assert not self.accepting and isinstance(msg, Message)
        data = msg.encode() + b"\x00"
        sock = self.sock
        self = None # avoid circular refs, not needed any more
        if ASYNC :
            await sock.sendall(data, timeout)
        else :
            sock.sendall(data, timeout)
        #end if
    #end _send

    async def send(self, msg, timeout = None) :
        "sends a message and waits for it to go out."
        if ASYNC :
            while len(self._send_queue) != 0 :
                # queue_send.send_queue_task is running, wait for it to finish
                if timeout != None :
                    deadline = AbsoluteTimeout(timeout)
                #end if
                if self._send_queue_empty == None :
                    my_wait = asyncio.get_running_loop().create_future()
                    self._send_queue_empty = my_wait
                else :
                    my_wait = None
                #end if
                w_self = weak_ref(self)
                awaiting = [self._send_queue_empty]
                if timeout != None :
                    awaiting.append(asyncio.sleep(deadline.timeout))
                #end if
                self = None
                await_any(awaiting)
                self = _wderef(w_self, "Connection")
                if not self._send_queue_empty.done() :
                    raise TimeoutError("send queue taking too long to empty")
                #end if
                if self._send_queue_empty == my_wait :
                    self._send_queue_empty = None
                #end if
                my_wait = None # might as well free it now
                if timeout != None :
                    timeout = deadline.timeout # time left
                #end if
            #end while
            return await self._send(msg, timeout)
        else :
            return self._send(msg, timeout)
        #end if
    #end send

    def queue_send(self, msg) :
        "puts a message on the outgoing queue, for delivery sometime hopefully soon." \
        " Asynchronous use only."

        if ASYNC :

            async def send_queue_task(w_self) :
                while True :
                    try :
                        self = _wderef(w_self, "Connection")
                        if not self.is_open :
                            break
                        try :
                            next = self._send_queue.pop(0)
                        except IndexError :
                            self._send_queue_task = None
                            break
                        #end try
                        self = None # try to minimize time strong ref is held
                        # TBD specify a timeout?
                        await asyncio.create_task(_wderef(w_self, "Connection")._send(next))
                    except (DanglingRefException, BrokenPipeError) :
                        break
                    #end try
                #end while
                self = _wderef(w_self, "Connection")
                if self._send_queue_empty != None and not self._send_queue_empty.done() :
                    self._send_queue_empty.set_result(None)
                #end if
            #end send_queue_task

            assert not self.accepting
            self._send_queue.append(msg)
            if self._send_queue_task == None or self._send_queue_task.done() :
                self._send_queue_task = asyncio.create_task(send_queue_task(weak_ref(self)))
            #end if

        else :
            raise NotImplementedError("only valid for async connections")
        #end if
    #end queue_send

    async def receive(self, timeout = None, methods_only = False) :
        assert not self.accepting
        w_self = weak_ref(self)
        self_sock = self.sock
        deadline = AbsoluteTimeout(timeout)
        result = None # to begin with
        while True :
            while True :
                items = self.rcvbuf.split(b"\00", 1)
                if len(items) == 2 :
                    self.rcvbuf = items[1]
                    result = Message.decode(items[0])
                    if methods_only and not result.is_method_call :
                        # TODO: option to log error?
                        result = None
                        self.close()
                    #end if
                    break
                #end if
                if self.eof :
                    break
                more = self_sock.recvimmed(IOBUFSIZE)
                if more == b"" :
                    self.eof = True
                    break
                #end if
                if more == None :
                    break
                self.rcvbuf += more
            #end while
            if not self.is_open or result != None or self.eof :
                break
            self = None # avoid circular refs while waiting
            if ASYNC :
                more = await self_sock.recv(IOBUFSIZE, deadline.timeout)
            else :
                more = self_sock.recv(IOBUFSIZE, deadline.timeout)
            #end if
            self = w_self()
            if self == None :
                break
            if more == None :
                raise TimeoutError("taking too long to receive")
            #end if
            self.rcvbuf += more
        #end while
        return \
            result
    #end receive

    def set_receiver_callback(self, receiver_callback, arg) :
        if ASYNC :

            async def receiver_task(w_self) :
                while True :
                    try :
                        msg = await asyncio.create_task(_wderef(w_self, "Connection").receive())
                    except DanglingRefException :
                        break
                    #end try
                    if msg == None : # assume EOF
                        break
                    receiver_callback(msg, arg)
                #end while
            #end receiver_task

            if self.receiver_task != None and not self.receiver_task.done() :
                self.receiver_task.cancel()
            #end if
            if receiver_callback != None :
                self.receiver_task = asyncio.create_task(receiver_task(weak_ref(self)))
            else :
                self.receiver_task = None
            #end if

        else :
            raise NotImplementedError("only valid for async connections")
        #end if
    #end set_receiver_callback

    async def transact(self, req, timeout = None) :
        "yields zero or more replies."
        assert not self.accepting and isinstance(req, Message)
        deadline = AbsoluteTimeout(timeout)
        if ASYNC :
            await self.send(req, deadline.timeout)
        else :
            self.send(req, deadline.timeout)
        #end if
        if not req.oneway :
            while True :
                if ASYNC :
                    reply = await self.receive(deadline.timeout)
                else :
                    reply = self.receive(deadline.timeout)
                #end if
                if reply == None :
                    break
                yield reply
                if reply.is_error or not (req.more and reply.continues) :
                    break
            #end while
        #end if
    #end transact

    async def transact1(self, req, timeout = None) :
        "expects no more than one reply."
        assert not self.accepting and isinstance(req, Message) and not req.more
        deadline = AbsoluteTimeout(timeout)
        if ASYNC :
            await self.send(req, deadline.timeout)
        else :
            self.send(req, deadline.timeout)
        #end if
        if req.oneway :
            result = None
        else :
            if ASYNC :
                reply = await self.receive(deadline.timeout)
            else :
                reply = self.receive(deadline.timeout)
            #end if
            # let caller check reply.continues flag if they care
        #end if
        return reply
    #end transact1

#end Connection

def_sync_async_classes(Connection, "Connection", "ConnectionAsync")

#+
# Message encoding/decoding
#-

class Message :
    "representation of a varlink message, with methods for encoding/decoding to/from string form."

    __slots__ = ("method", "parameters", "oneway", "more", "upgrade", "continues", "error")

    @classmethod
    def create_method(celf, method, parameters = None, *, validate = None, oneway = False, more = False, upgrade = False) :
        if validate != None :
            if not isinstance(validate, Interface.Method) :
                raise TypeError("validate arg must be an Interface.Method")
            #end if
            validate.validate_inargs(parameters, InvalidParamError)
        #end if
        assert not oneway or not more, "can’t specify no replies and more than one reply at once"
        self = celf()
        self.method = method
        self.parameters = parameters
        self.oneway = oneway
        self.more = more
        self.upgrade = upgrade
        self.continues = False
        self.error = None
        return self
    #end create_method

    @classmethod
    def create_reply(celf, parameters, *, validate = None, continues = False) :
        if validate != None :
            if not isinstance(validate, Interface.Method) :
                raise TypeError("validate arg must be an Interface.Method")
            #end if
            validate.validate_outargs(parameters, InvalidParamError)
        #end if
        self = celf()
        self.method = None
        self.parameters = parameters
        self.oneway = False
        self.more = False
        self.upgrade = False
        self.continues = continues
        self.error = None
        return self
    #end create_reply

    @classmethod
    def create_error(celf, error, parameters, *, validate = None) :
        if validate != None :
            if not isinstance(validate, Interface.Error) :
                raise TypeError("validate arg must be an Interface.Error")
            #end if
            validate.validate_inargs(parameters, InvalidParamError)
        #end if
        self = celf()
        self.method = None
        self.parameters = parameters
        self.oneway = False
        self.more = False
        self.upgrade = False
        self.continues = False
        self.error = error
        return self
    #end create_error

    def encode(self) :
        obj = {}
        if self.method != None :
            obj["method"] = self.method
        #end if
        if self.parameters != None :
            obj["parameters"] = self.parameters
        #end if
        if self.oneway :
            obj["oneway"] = True
        #end if
        if self.more :
            obj["more"] = True
        #end if
        if self.upgrade :
            obj["upgrade"] = True
        #end if
        if self.continues :
            obj["continues"] = True
        #end if
        if self.error != None :
            obj["error"] = self.error
        #end if
        return json.dumps(obj).encode()
    #end encode

    @classmethod
    def decode(celf, s) :
        obj = json.loads(s)
        self = celf()
        self.method = obj.get("method")
        self.parameters = obj.get("parameters")
        self.oneway = obj.get("oneway", False)
        self.more = obj.get("more", False)
        self.upgrade = obj.get("upgrade", False)
        self.continues = obj.get("continues", False)
        self.error = obj.get("error")
        return self
    #end decode

    @property
    def is_method_call(self) :
        "is this a method-call message."
        return self.method != None
    #end is_method_call

    @property
    def is_reply(self) :
        "is this a reply message."
        return self.method == None
    #end is_reply

    @property
    def is_error(self) :
        "is this reply an error reply."
        assert self.method == None, "not a reply message"
        return self.error != None
    #end is_error

    def to_error(self) :
        "returns an ErrorReturn object which can be raised to report this error."
        if not self.is_error :
            raise TypeError("only applicable to error returns")
        #end if
        return \
            ErrorReturn(self.error, self.parameters)
    #end to_error

    def __str__(self) :
        return \
            (
                "%(class)s(%(kind)s, %(args)s, oneway=%(oneway)s, more=%(more)s,"
                " upgrade=%(upgrade)s, continues=%(continues)s)"
            %
                {
                    "class" : type(self).__name__,
                    "kind" :
                        (
                            lambda : "reply",
                            lambda : "error:%s" % self.error,
                            lambda : "method:%s" % self.method,
                        )[int(self.error != None) + 2 * int(self.method != None)](),
                    "args" : repr(self.parameters),
                    "oneway" : self.oneway,
                    "more" : self.more,
                    "upgrade" : self.upgrade,
                    "continues" : self.continues,
                }
            )
    #end __str__

#end Message

#+
# Interface representation
#-

_interface_match = lambda rest : "[A-Za-z" + ("", "0-9")[rest] + "](?:-*[A-Za-z0-9])*"

def is_valid_interface_name(name) :
    return \
        (
            re.fullmatch
              (
                _interface_match(False) + "(?:\\." + _interface_match(True) + ")*",
                name
              )
        !=
            None
        )
#end is_valid_interface_name

def validate_interface_name(name) :
    "validates a name to be used to identify a Varlink interface."
    if not is_valid_interface_name(name) :
        raise SyntaxError("invalid Varlink interface name “%s”" % name)
    #end if
    return name
#end validate_interface_name

def is_valid_interface_name_component(name, rest : bool) :
    return re.fullmatch(_interface_match(rest), name) != None
#end is_valid_interface_name_component

def validate_interface_name_component(name, rest : bool) :
    "validates a component (a part between dots) of a Varlink interface name."
    if not is_valid_interface_name_component(name, rest) :
        raise SyntaxError \
            (
                    "invalid Varlink interface name %s component “%s”"
                %
                    (("initial", "rest")[rest], name)
            )
    #end if
    return name
#end validate_interface_name_component

def is_valid_name(name) :
    return re.fullmatch("[A-Z][A-Za-z0-9]*", name) != None
#end is_valid_name

def validate_name(name) :
    "validates a name to be used to identify a Varlink type, method or error."
    if not is_valid_name(name) :
        raise SyntaxError("invalid Varlink name “%s”" % name)
    #end if
    return name
#end validate_name

def is_valid_qualified_name(name) :
    items = name.rsplit(".", 1)
    return \
        (
            len(items) == 2
        and
            is_valid_interface_name(items[0])
        and
            is_valid_name(items[1])
        )
#end is_valid_qualified_name

def validate_qualified_name(name) :
    if not is_valid_qualified_name(name) :
        raise SyntaxError("invalid qualified Varlink name “%s”" % name)
    #end if
    return name
#end validate_qualified_name

def split_qualified_name(name) :
    "returns a («interface», «member_name») pair."
    return validate_qualified_name(name).rsplit(".", 1)
#end split_qualified_name

def is_valid_field_name(name) :
    return re.fullmatch("[A-Za-z](?:_?[A-Za-z0-9])*", name) != None
#end is_valid_field_name

def validate_field_name(name) :
    "validates a name to be used to identify a struct field, an" \
    " enum instance, or an argument of a method or error."
    if not is_valid_field_name(name) :
        raise SyntaxError("invalid Varlink field/enum/arg name “%s”" % name)
    #end if
    return name
#end validate_field_name

class InvalidParamError(TypeError) :
    "convenient wrapper around TypeError, which is intended to be" \
    " interchangeable with InvalidParamErrorReturn (below). This exception" \
    " is intended to indicate a bug in the caller’s code."

    def __init__(self, parameter) :
        msg = "InvalidParameter(parameter = %s)" % parameter
        super().__init__((msg,))
    #end __init__

#end InvalidParamError

class ErrorReturn(Exception) :
    "an error which is intended to be caught and converted to an" \
    " error reply Message. name is the fully-qualified error name," \
    " and args is the dict specifying the arguments for the error Message."

    def __init__(self, name, args) :
        self.errname = validate_qualified_name(name)
        self.errargs = args
        self.args = (name, args)
    #end __init__

    def to_message(self, validate = None) :
        "generates a suitably-formatted error Message using the name" \
        " and arguments from this ErrorReturn, optionally validating" \
        " the argument list according to validate, which should be an" \
        " Interface.Error object."
        return Message.create_error \
          (
            error = self.errname,
            parameters = self.errargs,
            validate = validate
          )
    #end to_message

#end ErrorReturn

class InterfaceErrorReturn(ErrorReturn) :
    "used to return one of the standard error names defined in" \
    " a given interface. Unlike the base class, name is given as" \
    " unqualified and is implicitly qualified with the name of the" \
    " interface; the result must name a valid Error definition," \
    " which will be used to validate the args."

    INTERFACE = None # override with appropriate Interface object in subclass

    def __init__(self, name, args) :
        interface = type(self).INTERFACE
        entry = interface.members_by_name[name]
        if not isinstance(entry, Interface.Error) :
            raise TypeError("not a standard error name: %s" % name)
        #end if
        super().__init__(interface.name + "." + name, args)
        self.validate = entry
    #end __init__

    def to_message(self) :
        return super().to_message(self.validate)
    #end to_message

#end InterfaceErrorReturn

class Type :
    "Note that the param_error arg to the validate() method is intended" \
    " to be either InvalidParamError or InvalidParamErrorReturn; the latter" \
    " is for validation of incoming message args, indicating that an error" \
    " message is supposed to be returned to a connection client, while the" \
    " former is for validation of outgoing message args, and indicates a bug" \
    " in the caller’s code."

    __slots__ = ("name",)

    def __init__(self, name) :
        self.name = name
    #end __init__

    def validate(self, val, param_error) :
        raise NotImplementedError \
            ("validation of %s type must be overridden by subclass" % self.name)
    #end validate

#end Type

class BuiltinType(Type) :

    __slots__ = ("python_type",)

    def __init__(self, name, python_type) :
        super().__init__(name)
        if not isinstance(python_type, type) :
            raise TypeError("python_type must be a Python type")
        #end if
        self.python_type = python_type
    #end __init__

    def __repr__(self) :
        return self.name
    #end __repr__

    def validate(self, val, param_error) :
        if not isinstance(val, self.python_type) :
            raise param_error \
                ("value %s is not compatible with type %s" % (repr(val), self.name))
        #end if
        return \
            val
    #end validate

#end BuiltinType :

builtin_types_by_name = {}
builtin_types_by_type = {}

for n, pt in \
    (
        ("bool", bool),
        ("int", int),
        ("float", float),
        ("string", str),
        ("object", bytes),
    ) \
:
    t = BuiltinType(n, pt)
    builtin_types_by_name[n] = t
    builtin_types_by_type[pt] = t
#end for
del n, pt, t

def get_type(t) :
    if isinstance(t, Type) :
        res = t
    elif isinstance(t, type) :
        res = builtin_types_by_type.get(t)
        if res == None :
            raise TypeError("no builtin Varlink equivalent to Python type “%s”" % t.__name__)
        #end if
    elif isinstance(t, str) :
        if t in builtin_types_by_name :
            res = builtin_types_by_name[t]
        elif is_valid_name(t) :
            # assume reference to (possibly not-yet-defined) user-defined type
            res = t
        else :
            raise NameError("unknown type “%s”" % t)
        #end if
    else :
        raise TypeError("not a Type or name of a type: %s" % repr(t))
    #end if
    return res
#end get_type

class UserType(Type) :
    "base type of all subtypes of Type other than BuiltinType, to do" \
    " name validation."

    def __init__(self, name) :
        super().__init__((lambda : None, lambda : validate_name(name))[name != None]())
    #end __init__

#end UserType

class NullableType(UserType) :
    "a type which can have a value of null, in addition to" \
    " the actual values of a specified target type."

    __slots__ = ("targtype",)

    def __init__(self, name, targtype) :
        super().__init__(name)
        self.targtype = get_type(targtype)
    #end __init__

    def __repr__(self) :
        return "? %s" % repr(self.targtype)
    #end __repr__

    def validate(self, val, param_error) :
        if val != None :
            self.targtype.validate(val, param_error)
        #end if
        return \
            val
    #end validate

#end NullableType

class ArrayType(UserType) :

    __slots__ = ("elttype",)

    def __init__(self, name, elttype) :
        super().__init__(name)
        self.elttype = get_type(elttype)
    #end __init__

    def __repr__(self) :
        return "[] %s" % repr(self.elttype)
    #end __repr__

    def validate(self, val, param_error) :
        if not isinstance(val, (list, tuple)) :
            raise param_error \
              (
                "need list or tuple for array of %s" % self.elttype.name
              )
        #end if
        for v in val :
            self.elttype.validate(v, param_error)
        #end for
        return \
            val
    #end validate

#end ArrayType

class DictType(UserType) :
    "key type is always string."

    __slots__ = ("valuetype")

    def __init__(self, name, valuetype) :
        super().__init__(name)
        self.valuetype = get_type(valuetype)
    #end __init__

    def __repr__(self) :
        return "[string] %s" % repr(self.valuetype)
    #end __repr__

    def validate(self, val, param_error) :
        if not isinstance(val, dict) or not (isinstance(k, str) for k in dict) :
            raise param_error \
              (
                "need dict with all-string keys for dict of %s" % self.valuetype.name
              )
        #end if
        for v in val.values() :
            self.valuetype.validate(v, param_error)
        #end for
        return \
            val
    #end validate

#end DictType

class Interface :

    __slots__ = ("name", "types", "methods", "errors", "members_by_name")

    class Arg :

        __slots__ = ("name", "type")

        def __init__(self, name, type) :
            self.name = validate_field_name(name)
            self.type = get_type(type)
        #end __init__

        def __repr__(self) :
            return \
                (
                    "%(name)s : %(type)s"
                %
                    {
                        "name" : self.name,
                        "type" : repr(self.type),
                    }
                )
        #end __repr__

        @staticmethod
        def validate_arglist(name, args, arg_names, vals, param_error) :
            # arg_names is optional, for quicker checking against valid arg names.
            if vals != None :
                if (
                        not isinstance(vals, dict)
                    or
                        not all(isinstance(k, str) for k in vals)
                ) :
                    raise param_error \
                      (
                        "need dict with recognized keys for %s" % name
                      )
                #end if
                if arg_names == None :
                    arg_names = set(a.name for a in args)
                #end if
                missing_keys = set \
                  (
                    f.name
                    for f in args
                    if not isinstance(f.type, NullableType) and f.name not in vals
                  )
                extra_keys = set \
                  (
                    f
                    for f in vals
                    if f not in arg_names
                  )
                errs = []
                if len(missing_keys) != 0 :
                    errs.append \
                      (
                        "missing required argument(s) for %s: %s" % (name, ", ".join(sorted(missing_keys)))
                      )
                #end if
                if len(extra_keys) != 0 :
                    errs.append \
                      (
                        "unexpected extra argument(s) for %s: %s" % (name, ", ".join(sorted(extra_keys)))
                      )
                #end if
                if len(errs) != 0 :
                    raise param_error("; ".join(errs))
                #end if
                for f in args :
                    if f.name in vals :
                        f.type.validate(vals[f.name], param_error)
                    else :
                        # fill in nullable value
                        vals[f.name] = None
                    #end if
                #end for
            else :
                if len(args) != 0 :
                    raise param_error("need nonempty field/arg list for %s" % name)
                #end if
                vals = {}
            #end if
            return vals
        #end validate_arglist

    #end Arg

    class StructType(UserType) :

        __slots__ = ("fields", "fields_by_name")

        def __init__(self, name, fields) :
            super().__init__(name)
            if (
                    not isinstance(fields, (tuple, list))
                or
                    not all(isinstance(f, Interface.Arg) for f in fields)
            ) :
                raise TypeError("fields must be sequence of Arg")
            #end if
            self.fields = list(fields)
            self.fields_by_name = dict((f.name, f) for f in fields)
        #end __init__

        def __repr__(self) :
            return \
                (
                    "type %s (" % self.name
                +
                    ", ".join(repr(f) for f in self.fields)
                +
                    ")"
                )
        #end __repr__

        def validate(self, val, param_error) :
            return \
                Interface.Arg.validate_arglist \
                  (
                    name = "fields of %s" % self.name,
                    args = self.fields,
                    arg_names = self.fields_by_name,
                    vals = val,
                    param_error = param_error
                  )
        #end validate

    #end StructType

    class EnumType(UserType) :

        __slots__ = ("values", "values_by_name")

        def __init__(self, name, values) :
            super().__init__(name)
            if (
                    not isinstance(values, (tuple, list))
                or
                    not all(isinstance(v, str) for v in values)
            ) :
                raise TypeError("values must be sequence of str")
            #end if
            self.values = list(validate_field_name(v) for v in values)
            self.values_by_name = dict((v, i) for i, v in enumerate(values))
        #end __init__

        def __repr__(self) :
            return \
                (
                    "type %s (" % self.name
                +
                    ", ".join(repr(n) for n in self.values)
                +
                    ")"
                )
        #end __repr__

        def validate(self, val, param_error) :
            if not isinstance(val, str) or val not in self.values_by_name :
                raise param_error("invalid enum value %s for type %s" % (repr(val), self.name))
            #end if
            return \
                val
        #end validate

    #end EnumType

    class Method :

        __slots__ = ("name", "inargs", "outargs", "oneway", "more", "inargs_by_name", "outargs_by_name")

        def __init__(self, name, inargs, outargs, oneway = False, more = False) :
            if not all \
              (
                isinstance(args, (tuple, list)) and all(isinstance(a, Interface.Arg) for a in args)
                for args in (inargs, outargs)
              ) \
            :
                raise TypeError("inargs and outargs must be sequences of Arg")
            #end if
            self.name = validate_name(name)
            self.inargs = list(inargs)
            self.outargs = list(outargs)
            if oneway and more :
                raise TypeError("cannot have both oneway and more")
            #end if
            self.oneway = oneway
            self.more = more
            self.inargs_by_name = dict((a.name, a) for a in inargs)
            self.outargs_by_name = dict((a.name, a) for a in outargs)
        #end __init__

        def __repr__(self) :
            return \
                (
                    "method %(name)s %(inargs)s -> %(outargs)s"
                %
                    {
                        "name" : self.name,
                        "inargs" : "(" + ", ".join(repr(a) for a in self.inargs) + ")",
                        "outargs" : "(" + ", ".join(repr(a) for a in self.outargs) + ")",
                    }
                )
        #end __repr__

        def validate_inargs(self, inargs, param_error) :
            return \
                Interface.Arg.validate_arglist \
                  (
                    name = "inargs of method %s" % self.name,
                    args = self.inargs,
                    arg_names = self.inargs_by_name,
                    vals = inargs,
                    param_error = param_error
                  )
        #end validate_inargs

        def validate_outargs(self, outargs, param_error) :
            return \
                Interface.Arg.validate_arglist \
                  (
                    name = "outargs of method %s" % self.name,
                    args = self.outargs,
                    arg_names = self.outargs_by_name,
                    vals = outargs,
                    param_error = param_error
                  )
        #end validate_outargs

    #end Method

    class Error :

        __slots__ = ("name", "inargs", "inargs_by_name")

        def __init__(self, name, inargs) :
            if (
                    not isinstance(inargs, (tuple, list))
                or
                    not all(isinstance(a, Interface.Arg) for a in inargs)
            ) :
                raise TypeError("inargs must be sequence of Arg")
            #end if
            self.name = validate_name(name)
            self.inargs = list(inargs)
            self.inargs_by_name = dict((a.name, a) for a in inargs)
        #end __init__

        def __repr__(self) :
            return \
                (
                    "error %(name)s %(inargs)s"
                %
                    {
                        "name" : self.name,
                        "inargs" : "(" + ", ".join(repr(a) for a in self.inargs) + ")",
                    }
                )
        #end __repr__

        def validate_inargs(self, inargs, param_error) :
            return \
                Interface.Arg.validate_arglist \
                  (
                    name = "inargs of error %s" % self.name,
                    args = self.inargs,
                    arg_names = self.inargs_by_name,
                    vals = inargs,
                    param_error = param_error
                  )
        #end validate_inargs

    #end Error

    def __init__(self, name, types, methods, errors) :

        dangling_types = None

        def resolve_type(t) :
            if isinstance(t, str) :
                if t in self.members_by_name :
                    t = self.members_by_name[t]
                else :
                    dangling_types.add(t)
                #end if
            #end if
            return t
        #end resolve_type

        def resolve_arglist(arglist) :
            for arg in arglist :
                arg.type = resolve_type(arg.type)
            #end for
        #end resolve_arglist

    #begin __init__
        Interface = type(self)
        for argname, arg, elttype in \
            (
                ("types", types, Type),
                ("methods", methods, Interface.Method),
                ("errors", errors, Interface.Error),
            ) \
        :
            if (
                    not isinstance(arg, (list, tuple))
                or
                    not all(isinstance(elt, elttype) for elt in arg)
            ) :
                raise TypeError("%s must be sequence of %s" % (argname, elttype.__name__))
            #end if
        #end for
        self.name = validate_interface_name(name)
        self.types = list(types)
        self.methods = list(methods)
        self.errors = list(errors)
        self.members_by_name = dict \
          (
            (e.name, e)
            for e in types + methods + errors
          )
        dangling_types = set()
        for t in self.types :
            if isinstance(t, NullableType) :
                t.targtype = resolve_type(t.targtype)
            elif isinstance(t, ArrayType) :
                t.elttype = resolve_type(t.elttype)
            elif isinstance(t, DictType) :
                t.valuetype = resolve_type(t.valuetype)
            elif isinstance(t, Interface.StructType) :
                resolve_arglist(t.fields)
            #end if
        #end for
        for m in self.methods :
            resolve_arglist(m.inargs)
            resolve_arglist(m.outargs)
        #end for
        for e in self.errors :
            resolve_arglist(e.inargs)
        #end for
        if len(dangling_types) != 0 :
            raise NameError("references to undefined types: %s" % ", ".join(sorted(dangling_types)))
        #end if
    #end __init__

    def __repr__(self) :
        return \
            (
                "%(name)s(types = %(types)s, methods = %(methods)s, errors = %(errors)s)"
            %
                {
                    "name" : type(self).__name__,
                    "types" :
                        "[" + ", ".join(repr(t) for t in self.types) + "]",
                    "methods" :
                            "["
                        +
                            ", ".join(repr(m) for m in self.methods)
                        +
                            "]",
                    "errors" :
                            "["
                        +
                            ", ".join(repr(e) for e in self.errors)
                        +
                            "]",
                }
            )
    #end __repr__

    class _Parse_Interface :

        # note about limitations of Arpeggio: for grammars expressed as
        # Python functions, these must be actual named def objects, not
        # lambdas and not bound class/instance methods. So I have given
        # up and am using string-based PEG syntax instead.

        # Note that I don’t require the parts of an interface definition
        # to actually start on new lines.
        alpha = "A-Za-z"
        digits = "0-9"
        alphanum = alpha + digits
        ifident = \
            (lambda alpha, digits, alphanum :
                lambda rest : "[" + alpha + ("", digits)[rest] + "](-*[" + alphanum + "])*"
            )(alpha, digits, alphanum)
        rules = \
          (
            "ifname <- r'" + ifident(False) + "(\\." + ifident(True) + ")*';\n"
            "name <- r'[A-Z][" + alphanum + "]*';\n"
            "fieldname <- r'[" + alpha + "](_?[" + alphanum + "])*';\n"
            "enumname <- r'[" + alpha + "](_?[" + alphanum + "])*';\n"
            "typeref <- r'[" + alpha + "][" + alphanum + "]*';\n"
            "\n"
            "fielddef <- fieldname \":\" typedef;\n"
            "structdef <- \"(\" \")\" / \"(\" fielddef (\",\" fielddef)* \")\";\n"
            "enumdef <- \"(\" enumname (\",\" enumname)* \")\";\n"
            "\n"
            "elttypedef <- structdef / enumdef / typeref / " +
                " / ".join("\"%s\"" % t for t in builtin_types_by_name.keys()) + ";\n"
            "typedef <- elttypedef / \"[]\" typedef / \"[string]\" typedef /"
                " (\"?\" (typedef / \"[]\" typedef / \"[string]\" typedef));\n"
            "\n"
            "typedecl <- \"type\" name typedef;\n"
            "methoddecl <- \"method\" name structdef \"->\" structdef;\n"
            "errordecl <- \"error\" name structdef;\n"
            "\n"
            "member <- typedecl / methoddecl / errordecl;\n"
            "interface <- \"interface\" ifname member+ EOF;\n"
          )

        @classmethod
        def _parse_interface(celf, Interface, s) :

            token = None
            tokens_stack = []

            def pushtokens() :
                nonlocal tokens
                tokens_stack[:0] = [tokens]
                tokens = list(token)
                nexttoken()
            #end pushtokens

            def poptokens() :
                nonlocal tokens
                nexttoken()
                assert token == None, "leftover token %s" % repr(token)
                tokens = tokens_stack.pop(0)
            #end poptokens

            def nexttoken() :
                nonlocal token
                if len(tokens) != 0 :
                    token = tokens.pop(0)
                else :
                    token = None
                #end if
            #end nexttoken

            def parse_fields() :
                assert token.value == "("
                nexttoken()
                fields = []
                if token.value != ")" :
                    while True :
                        if token == None :
                            break
                        assert token.rule_name == "fielddef"
                        pushtokens()
                        assert token.rule_name == "fieldname"
                        fieldname = token.value
                        nexttoken()
                        assert token.value == ":"
                        nexttoken()
                        fieldtype = parse_typedef(None)
                        poptokens()
                        fields.append(Interface.Arg(name = fieldname, type = fieldtype))
                        nexttoken()
                        assert token.value in (",", ")")
                        nexttoken()
                    #end while
                else :
                    # empty field list
                    nexttoken()
                #end if
                return fields
            #end parse_fields

            def parse_typedef(name) :

                def parse_elttypedef(name) :
                    assert token.rule_name == "elttypedef"
                    pushtokens()
                    if token.rule_name == "enumdef" :
                        pushtokens()
                        assert token.value == "("
                        nexttoken()
                        enums = []
                        while True :
                            enums.append(token.value)
                            nexttoken()
                            if token.value != "," :
                                break
                            nexttoken()
                            assert token.rule_name == "enumname"
                        #end while
                        result = Interface.EnumType(name, enums)
                        assert token.value == ")"
                        nexttoken()
                        poptokens()
                    elif token.rule_name == "structdef" :
                        pushtokens()
                        fields = parse_fields()
                        result = Interface.StructType(name, fields)
                        poptokens()
                    elif token.rule_name == "typeref" :
                        type_name = token.value
                        if type_name not in types_by_name :
                            raise NameError("reference to undefined type %s" % type_name)
                        #end if
                        result = types_by_name[type_name]
                        nexttoken()
                    else :
                        raise AssertionError("unrecognized type rule %s" % token.rule_name)
                    #end if
                    poptokens()
                    return result
                #end parse_elttypedef

            #begin parse_typedef
                assert token.rule_name == "typedef"
                pushtokens()
                nullable = False
                if token.value == "?" :
                    nullable = True
                    nexttoken()
                #end if
                if token.value == "[]" :
                    nexttoken()
                    valtype = parse_typedef(None)
                    result = ArrayType(name, valtype)
                elif token.value == "[string]" :
                    nexttoken()
                    valtype = parse_typedef(None)
                    result = DictType(name, valtype)
                elif token.rule_name == "elttypedef" :
                    result = parse_elttypedef(name)
                elif token.rule_name == "typedef" :
                    result = parse_typedef(name)
                else :
                    raise AssertionError("unrecognized typedef token %s: %s" % (token.rule_name, repr(token.value)))
                #end if
                if nullable :
                    result = NullableType(name, result)
                #end if
                if name != None :
                    if name in names_seen :
                        raise NameError("duplicate name %s" % name)
                    #end if
                    names_seen.add(name)
                #end if
                poptokens()
                return result
            #end parse_typedef

            def parse_arglist() :
                assert token.rule_name == "structdef"
                pushtokens()
                args = parse_fields()
                poptokens()
                nexttoken()
                return args
            #end parse_arglist

        #begin _parse_interface
            s = "\n".join \
              (
                re.sub("\\#[^\n\r\u2028\u2029]*$", "", l)
                for l in re.split("[\n\r\u2028\u2029]", s)
              ) # strip comments
            parser = arp.peg.ParserPEG(celf.rules, "interface")
            intfdef = parser.parse(s)
            assert intfdef[1].rule_name == "ifname" and intfdef[-1].rule_name == "EOF"
            ifname = intfdef[1].value
            tokens = list(intfdef[2:-1])
            types = []
            types_by_name = dict((n, t) for n, t in builtin_types_by_name.items())
            methods = []
            errors = []
            names_seen = set()
            nexttoken()
            while token != None and token.rule_name == "member" :
                pushtokens()
                if token.rule_name == "typedecl" :
                    pushtokens()
                    assert token.value == "type"
                    nexttoken()
                    assert token.rule_name == "name"
                    typename = token.value
                    nexttoken()
                    this_type = parse_typedef(typename)
                    types.append(this_type)
                    types_by_name[typename] = this_type
                    poptokens()
                elif token.rule_name == "methoddecl" :
                    pushtokens()
                    assert token.value == "method"
                    nexttoken()
                    item_name = token.value
                    nexttoken()
                    inargs = parse_arglist()
                    assert token.value == "->"
                    nexttoken()
                    outargs = parse_arglist()
                    this_method = Interface.Method \
                      (
                        name = item_name,
                        inargs = inargs,
                        outargs = outargs
                      )
                    if item_name in names_seen :
                        raise NameError("duplicate name %s" % item_name)
                    #end if
                    names_seen.add(item_name)
                    methods.append(this_method)
                    poptokens()
                elif token.rule_name == "errordecl" :
                    pushtokens()
                    assert token.value == "error"
                    nexttoken()
                    item_name = token.value
                    nexttoken()
                    inargs = parse_arglist()
                    this_error = Interface.Error \
                      (
                        name = item_name,
                        inargs = inargs
                      )
                    if item_name in names_seen :
                        raise NameError("duplicate name %s" % name)
                    #end if
                    names_seen.add(item_name)
                    errors.append(this_error)
                    poptokens()
                else :
                    raise AssertionError("unrecognized item rule %s" % token.rule_name)
                #end if
                poptokens()
                nexttoken()
            #end while
            return \
                Interface \
                  (
                    name = ifname,
                    types = types,
                    methods = methods,
                    errors = errors
                  )
        #end _parse_interface

    #end _Parse_Interface

    _parse_interface = _Parse_Interface._parse_interface

    del _Parse_Interface

    @classmethod
    def from_string(celf, s) :
        return celf._parse_interface(celf, s)
    #end from_string

    def to_string(self) :

        Interface = type(self)
        types_ref = set(n for n in builtin_types_by_name)

        def write_arg(out, arg) :
            out.write(arg.name)
            out.write(" : ")
            write_type(out, arg.type, False)
        #end write_arg

        def write_args(out, args, toplevel) :
            if toplevel :
                out.write("\n  (\n")
            else :
                out.write("(")
            #end if
            first = True
            for f in args :
                if first :
                    first = False
                else :
                    out.write(",")
                    if toplevel :
                        out.write("\n")
                    else :
                        out.write(" ")
                    #end if
                #end if
                if toplevel :
                    out.write("    ")
                #end if
                write_arg(out, f)
            #end for
            if toplevel :
                if not first :
                    out.write("\n")
                #end if
                out.write("  )\n")
            else :
                out.write(")")
            #end if
        #end write_args

        def write_type(out, t, toplevel) :
            if t.name in types_ref :
                if toplevel :
                    out.write(" ")
                #end if
                out.write(t.name)
            else :
                if t.name != None :
                    types_ref.add(t.name)
                #end if
                if isinstance(t, Interface.StructType) :
                    write_args(out, t.fields, toplevel)
                elif isinstance(t, Interface.EnumType) :
                    out.write(" (%s)" % ", ".join(t.values))
                elif isinstance(t, NullableType) :
                    out.write("?")
                    write_type(out, t.targtype, toplevel)
                elif isinstance(t, ArrayType) :
                    out.write("[]")
                    write_type(out, t.elttype, toplevel)
                elif isinstance(t, DictType) :
                    out.write("[string]")
                    write_type(out, t.valuetype, toplevel)
                else :
                    raise ValueError("unexpected %s type %s" % (type(t).__name__, t.name))
                #end if
            #end if
        #end write_type

    #begin to_string
        out = io.StringIO()
        out.write("interface %s\n" % self.name)
        for t in self.types :
            out.write("\n")
            out.write("type %s" % t.name)
            write_type(out, t, True)
        #end for
        for m in self.methods :
            out.write("\nmethod %s " % m.name)
            write_args(out, m.inargs, False)
            out.write(" -> ")
            write_args(out, m.outargs, False)
            out.write("\n")
        #end for
        for e in self.errors :
            out.write("\nerror %s " % e.name)
            write_args(out, e.inargs, False)
            out.write("\n")
        #end for
        return out.getvalue()
    #end to_string

#end Interface

#+
# Standard interfaces
#-

standard_interfaces = \
    {
        "org.varlink.service" :
            Interface
              (
                name = "org.varlink.service",
                types = [],
                methods =
                    [
                        Interface.Method
                          (
                            name = "GetInfo",
                            inargs = [],
                            outargs =
                                [
                                    Interface.Arg("vendor", "string"),
                                    Interface.Arg("product", "string"),
                                    Interface.Arg("version", "string"),
                                    Interface.Arg("url", "string"),
                                    Interface.Arg
                                      (
                                        name = "interfaces",
                                        type = ArrayType(None, "string")
                                      ),
                                ],
                          ),
                        Interface.Method
                          (
                            name = "GetInterfaceDescription",
                            inargs =
                                [
                                    Interface.Arg("interface", "string"),
                                ],
                            outargs =
                                [
                                    Interface.Arg("description", "string"),
                                ],
                          ),
                    ],
                errors =
                    [
                        Interface.Error
                          (
                            name = "InterfaceNotFound",
                            inargs =
                                [
                                    Interface.Arg("interface", "string"),
                                ]
                          ),
                        Interface.Error
                          (
                            name = "MethodNotFound",
                            inargs =
                                [
                                    Interface.Arg("method", "string"),
                                ]
                          ),
                        Interface.Error
                          (
                            name = "MethodNotImplemented",
                            inargs =
                                [
                                    Interface.Arg("method", "string"),
                                ]
                          ),
                        Interface.Error
                          (
                            name = "InvalidParameter",
                            inargs =
                                [
                                    Interface.Arg("parameter", "string"),
                                ]
                          ),
                        Interface.Error
                          (
                            name = "PermissionDenied",
                            inargs = []
                          ),
                        Interface.Error
                          (
                            name = "ExpectedMore",
                            inargs = []
                          ),
                    ],
              ),
        "org.varlink.resolver" :
            # from <https://github.com/cherry-pick/com.redhat.resolver/blob/master/src/org.varlink.resolver.varlink>
            Interface
              (
                name = "org.varlink.resolver",
                types = [],
                methods =
                    [
                        Interface.Method
                          (
                            name = "GetInfo",
                            inargs = [],
                            outargs =
                                [
                                    Interface.Arg("vendor", "string"),
                                    Interface.Arg("product", "string"),
                                    Interface.Arg("version", "string"),
                                    Interface.Arg("url", "string"),
                                    Interface.Arg
                                      (
                                        name = "interfaces",
                                        type = ArrayType(None, "string")
                                      ),
                                ],
                          ),
                    ],
                errors =
                    [
                        Interface.Error
                          (
                            name = "InterfaceNotFound",
                            inargs =
                                [
                                    Interface.Arg("interface", "string"),
                                ]
                          ),
                    ]
              ),
    }

class ServiceErrorReturn(InterfaceErrorReturn) :
    "used to return one of the standard error names in" \
    " the org.varlink.service interface."

    INTERFACE = standard_interfaces["org.varlink.service"]

#end ServiceErrorReturn

class InvalidParamErrorReturn(ServiceErrorReturn) :
    "convenient wrapper around ServiceErrorReturn, which is intended to be" \
    " interchangeable with InvalidParamError (above). This exception is" \
    " intended to indicate a protocol violation error by the connection client."

    def __init__(self, parameter) :
        super().__init__("InvalidParameter", {"parameter" : parameter})
    #end __init__

#end InvalidParamErrorReturn

#+
# Tidy up
#-

del ConditionalExpander, def_sync_async_classes
  # your work is done
del inspect, ast # not needed any more either

def _atexit() :
    # disable all __del__ methods at process termination to avoid segfaults
    for cls in Connection, ConnectionAsync :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
