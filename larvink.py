"""
Pure-Python implementation of Varlink protocol <https://varlink.org/>.

This Python binding supports hooking into event loops via Python’s standard
asyncio module.
"""
#+
# Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import sys
import io
from weakref import \
    ref as weak_ref
import enum
import asyncio
import atexit
import darlink as dl
from darlink import \
    _wderef, \
    DanglingRefException, \
    ErrorReturn, \
    ServiceErrorReturn, \
    Interface, \
    Message

#+
# High-level bus connection
#-

class ConnectionAsync :
    "higher-level wrapper around darlink.ConnectionAsync, allowing for" \
    " RPC-style method dispatch. Note that class instantiation is async."

    __slots__ = \
        (
            "__weakref__",
            "conn",
            "_acceptor",
            "acceptance_filter",
              # callback which is invoked with new client connection
              # every time one is created.
            "_receiver",
            "_parent",
            "_clients",
            "dispatch",
            "closing",
        )

    async def __new__(celf, bind = None, connect = None, sock = None, timeout = None, parent = None) :
        if sum(int(a != None) for a in (bind, connect, sock)) != 1 :
            raise ValueError("specify exactly one of bind, connect or sock args")
        #end if
        self = super().__new__(celf)
        if sock != None :
            self.conn = sock
        else :
            self.conn = await dl.ConnectionAsync \
              (
                bind = bind,
                connect = connect,
                timeout = timeout
              )
        #end if
        self.acceptance_filter = None
        self.dispatch = {}
        self.closing = asyncio.get_running_loop().create_future()
        if self.conn.accepting :
            self._clients = []
            self._acceptor = asyncio.create_task(self._accept_clients(weak_ref(self)))
        else :
            self._clients = self._acceptor = None
        #end if
        if parent != None :
            self.dispatch = parent.dispatch
            self._parent = weak_ref(parent)
        else :
            self._parent = None
        #end if
        self._receiver = None
        return self
    #end __new__

    @property
    def is_open(self) :
        return \
            self.conn != None and self.conn.is_open
    #end is_open

    @staticmethod
    async def _accept_clients(w_self) :
        while True :
            try :
                conn = await asyncio.create_task(_wderef(w_self, "Connection").conn.accept())
                if conn == None :
                    break
                self = _wderef(w_self, "Connection")
                client = await type(self)(sock = conn, parent = self)
                client._receiver = asyncio.create_task(self._receive_requests(weak_ref(client)))
                self._clients.append(client)
                if self.acceptance_filter != None :
                    res = self.acceptance_filter(client)
                    if asyncio.iscoroutine(res) :
                        await res
                    #end if
                #end if
                self = None
            except DanglingRefException :
                break
            #end try
        #end while
    #end _accept_clients

    @staticmethod
    async def _receive_requests(w_self) :

        self = None
        while True :
            if not _wderef(w_self, "Connection").is_open :
                break
            try :
                msg = await asyncio.create_task \
                  (
                    _wderef(w_self, "Connection").conn.receive(methods_only = True)
                  )
                if msg == None :
                    _wderef(w_self, "Connection").close()
                    break
                #end if
                self = _wderef(w_self, "Connection")
            except DanglingRefException :
                break
            #end try
            try :
                to_return_result = None
                reply_continues = False
                try :
                    interface_name, method_name = dl.split_qualified_name(msg.method)
                    if self._parent != None :
                        dispatch = _wderef(self._parent, "Parent Connection").dispatch
                    else :
                        dispatch = self.dispatch
                    #end if
                    if interface_name not in dispatch :
                        raise ServiceErrorReturn("InterfaceNotFound", {"interface" : interface_name})
                    #end if
                    intf = self.dispatch[interface_name]
                    intf_type = type(intf)
                    entry = intf_type._interface_methods.get(method_name)
                    if entry == None :
                        raise ServiceErrorReturn("MethodNotFound", {"method" : method_name})
                    #end if
                    call_info = entry["action"]._method_info
                    try :
                        Interface.Arg.validate_arglist \
                          (
                            "inargs of %s" % msg.method,
                            call_info["inargs"], None,
                            msg.parameters,
                            dl.InvalidParamErrorReturn
                          )
                    except (TypeError, ValueError) as err :
                        raise ServiceErrorReturn("InvalidParameter", {"parameter" : str(err)})
                    #end try
                    if call_info["want_more"] and not msg.more :
                        # do after inargs validation, because why not
                        raise ServiceErrorReturn("ExpectedMore", {})
                    #end if
                    args = tuple \
                      (
                        msg.parameters.get(argdef.name)
                        for argdef in call_info["inargs"]
                      )
                    kwargs = {}
                    for keyword_keyword, value in \
                        (
                            ("connection_keyword", lambda : self),
                            ("message_keyword", lambda : msg),
                            ("oneway_keyword", lambda : msg.oneway),
                            ("more_keyword", lambda : msg.more),
                        ) \
                    :
                        if call_info[keyword_keyword] != None :
                            kwargs[call_info[keyword_keyword]] = value()
                        #end if
                    #end for
                    if call_info["inargs_keyword"] != None :
                        if call_info["inargs_keys"] != None :
                            args = dict \
                              (
                                (key, value)
                                for key, value in zip(call_info["inargs_keys"], args)
                              )
                            if "inargs_constructor" in call_info :
                                args = call_info["inargs_constructor"](**args)
                            #end if
                        #end if
                        kwargs[call_info["inargs_keyword"]] = args
                        args = ()
                    else :
                        if call_info["inargs_keys"] != None :
                            for key, value in zip(call_info["inargs_keys"], args) :
                                kwargs[key] = value
                            #end for
                            args = ()
                        #end if
                    #end if
                    # Note in all cases of multiple results below, I block processing
                    # of further incoming messages on this connection until this request
                    # is done.
                    result = getattr(intf, entry["name"])(*args, **kwargs)

                    def send_result(outargs, continues) :
                        outargs = Interface.Arg.validate_arglist \
                          (
                            "outargs of %s" % msg.method,
                            call_info["outargs"], None,
                            outargs,
                            dl.InvalidParamError
                          )
                        self.conn.queue_send \
                          (
                            Message.create_reply
                              (
                                parameters = outargs,
                                continues = continues
                              )
                          )
                    #end send_result

                    if hasattr(result, "__next__") :
                        resultiter = result
                        for result, continues in resultiter :
                            if not self.is_open :
                                break
                            send_result(result, continues)
                        #end for
                    elif hasattr(result, "__anext__") :
                        resultiter = result
                        async for result, continues in resultiter :
                            if not self.is_open :
                                break
                            send_result(result, continues)
                        #end for
                    else :
                        if asyncio.iscoroutine(result) :
                            result = await result
                        #end if
                        if result != None :
                            if msg.oneway :
                                raise RuntimeError("No method result expected")
                            #end if
                            send_result(result, False)
                        elif not msg.oneway :
                            raise RuntimeError("Missing required method result")
                        #end if
                    #end if
                except ErrorReturn as err :
                    self.conn.queue_send(err.to_message())
                #end if
            except SyntaxError as err :
                # bad incoming message
                sys.stderr.write("Bad message from %s: %s\n" % (self.conn.peer, str(err)))
                # TODO: option to log error?
                self.close()
            #end try
            self = None
        #end while
    #end _receive_requests

    def close(self) :
        if self.conn != None :
            if self._clients != None :
                for client in self._clients :
                    client.close()
                #end for
                self._acceptor.cancel()
                self._clients = self._acceptor = None
            #end if
            if self._receiver != None :
                try :
                    parent = _wderef(self._parent, "Parent Connection")
                    parent._clients.remove(self)
                except DanglingRefException :
                    pass
                #end try
                self._receiver.cancel()
                self._receiver = None
            #end if
            self.conn.close()
            self.conn = None
            if not self.closing.done() :
                self.closing.set_result(None)
            #end if
        #end if
    #end close

    def __del__(self) :
        self.close()
    #end __del__

    def register(self, intf) :
        if is_interface(intf) :
            intf_type = intf
            intf = intf_type()
        elif is_interface_instance(intf) :
            intf_type = type(intf)
        else :
            raise ValueError("intf arg must be an @interface class or instance")
        #end if
        interface_name = intf_type._interface_name
        # TODO: error/warning if name already taken?
        self.dispatch[interface_name] = intf
    #end register

    async def introspect(self, intf_name : str) :
        "returns a proxy interface class which can be used to call methods" \
        " on the specified interface, if the peer supports it."
        service = dl.standard_interfaces["org.varlink.service"]
        get_info = service.members_by_name["GetInterfaceDescription"]
        request = Message.create_method \
          (
            method = "org.varlink.service.GetInterfaceDescription",
            parameters = {"interface" : intf_name},
            validate = get_info
          )
        reply = await self.conn.transact1(request)
        if reply.is_error :
            raise reply.to_error()
        #end if
        intfstr = reply.parameters.get("description")
        if intfstr == None or not isinstance(intfstr, str) :
            raise ServiceErrorReturn("InvalidParameter", {"parameter" : "description"})
        #end if
        try :
            intf = Interface.from_string(intfstr)
        except (SyntaxError, dl.arp.NoMatch, NameError) :
            raise ServiceErrorReturn("InvalidParameter", {"parameter" : "description"})
        #end try
        return \
            def_proxy_interface(None, intf)
    #end introspect

#end ConnectionAsync

#+
# Interface-dispatch mechanism
#-

def def_attr_class(name, attrs) :
    "defines a class with read/write attributes with names from the sequence attrs." \
    " Objects of this class can be coerced to lists or tuples, and attributes can" \
    " also be accessed by index, like a list."

    class result :
        __slots__ = tuple(attrs)

        def __init__(self, **kwargs) :
            for name in type(self).__slots__ :
                setattr(self, name, None)
            #end for
            for name in kwargs :
                setattr(self, name, kwargs[name])
            #end for
        #end __init__

        def __repr__(self) :
            return \
                (
                        "%s(%s)"
                    %
                        (
                            type(self).__name__,
                            ", ".join
                              (
                                    "%s = %s"
                                %
                                    (name, repr(getattr(self, name)))
                                    for name in type(self).__slots__
                              ),
                        )
                )
        #end __repr__

        def __len__(self) :
            return \
                len(type(self).__slots__)
        #end __len__

        def __getitem__(self, i) :
            return \
                getattr(self, type(self).__slots__[i])
        #end __getitem__

        def __setitem__(self, i, val) :
            setattr(self, type(self).__slots__[i], val)
        #end __setitem__

    #end class

#begin def_attr_class
    result.__name__ = name
    return \
        result
#end def_attr_class

def interface(name) :

    def decorate(celf) :

        class InterfaceErrorReturn(dl.ErrorReturn) :
            pass
        #end InterfaceErrorReturn

        dangling_types = None

        def resolve_type(t) :
            if isinstance(t, str) :
                if t in celf._interface_types :
                    t = celf._interface_types[t]
                else :
                    dangling_types.add(t)
                #end if
            #end if
            return t
        #end resolve_type

        def resolve_arglist(arglist) :
            for arg in arglist :
                arg.type = resolve_type(arg.type)
            #end for
        #end resolve_arglist

    #begin decorate
        if not isinstance(celf, type) :
            raise TypeError("only apply decorator to classes.")
        #end if
        if name.endswith(".") :
            interface_name = name + celf.__name__
        else :
            interface_name = name
        #end if
        celf._interface_name = dl.validate_interface_name(interface_name)
        celf._interface_types = \
            dict \
              (
                (t._type_info["name"], t)
                for tname in dir(celf)
                for t in (getattr(celf, tname),)
                if hasattr(t, "_type_info")
              )
        celf._interface_methods = \
            dict \
              (
                (f._method_info["name"], {"name" : fname, "action" : f})
                for fname in dir(celf)
                for f in (getattr(celf, fname),)
                if hasattr(f, "_method_info")
              )
        celf._interface_errors = \
            dict \
              (
                (f._error_info["name"], {"name" : fname, "action" : f})
                for fname in dir(celf)
                for f in (getattr(celf, fname),)
                if hasattr(f, "_error_info")
              )
        if len(celf._interface_errors) != 0 :
            InterfaceErrorReturn.__name__ = celf.__name__ + "_error_return"
            InterfaceErrorReturn.__doc__ = "for returning errors from %s interface" % name
            celf._interface_error_return = InterfaceErrorReturn
        #end if
        dangling_types = set()
        for t in celf._interface_types.values() :
            if isinstance(t, dl.NullableType) :
                t.targtype = resolve_type(t.targtype)
            elif isinstance(t, dl.ArrayType) :
                t.elttype = resolve_type(t.elttype)
            elif isinstance(t, dl.DictType) :
                t.valuetype = resolve_type(t.valuetype)
            elif isinstance(t, Interface.StructType) :
                resolve_arglist(t.fields)
            #end if
        #end for
        for m in celf._interface_methods.values() :
            resolve_arglist(m["action"]._method_info["inargs"])
            resolve_arglist(m["action"]._method_info["outargs"])
        #end for
        for e in celf._interface_errors.values() :
            resolve_arglist(e["action"]._error_info["inargs"])
        #end for
        if len(dangling_types) != 0 :
            raise NameError("references to undefined types: %s" % ", ".join(sorted(dangling_types)))
        #end if
        return celf
    #end decorate

#begin interface
    return decorate
#end interface

def is_interface(cłass) :
    "is cłass defined as an interface class."
    return \
        isinstance(cłass, type) and hasattr(cłass, "_interface_name")
#end is_interface

def is_interface_instance(obj) :
    "is obj an instance of an interface class."
    return \
        is_interface(type(obj))
#end is_interface_instance

class USERTYPEKIND(enum.Enum) :
    "kinds of user-defined types."
    STRUCT = 1
    ENUM = 2
#end USERTYPEKIND

def get_field_defs(fields, what) :

    def map_field_type(fieldtype) :
        if isinstance(fieldtype, str) :
            if fieldtype in dl.builtin_types_by_name or dl.is_valid_name(fieldtype) :
                pass # return same
            else :
                raise TypeError("invalid %s type name “%s”" % (what, fieldtype))
            #end if
        elif isinstance(fieldtype, type) :
            if fieldtype in dl.builtin_types_by_type :
                fieldtype = dl.builtin_types_by_type[fieldtype]
            elif hasattr(fieldtype, "_type_info") :
                type_info = fieldtype._type_info
                if type_info["kind"] == USERTYPEKIND.STRUCT :
                    fieldtype = Interface.StructType \
                      (
                        name = type_info["name"],
                        fields = type_info["fields"]
                      )
                elif type_info["kind"] == USERTYPEKIND.ENUM :
                    fieldtype = Interface.EnumType \
                      (
                        name = type_info["name"],
                        values = type_info["values"]
                      )
                else :
                    raise TypeError \
                      (
                            "unexpected %s type %s kind %s"
                        %
                            (what, fieldtype.__name__), repr(type_info)
                      )
                #end if
            else :
                raise TypeError("unusable %s type %s" % (what, fieldtype.__name__))
            #end if
        else :
            assert isinstance(fieldtype, dl.Type)
        #end if
        return fieldtype
    #end map_field_type

#begin get_field_defs
    bad = False # to begin with
    if isinstance(fields, (list, tuple)) :
        if all(isinstance(f, Interface.Arg) for f in fields) :
            pass # fine
        elif all \
          (
                isinstance(f, (list, tuple))
            and
                len(f) == 2
            and
                isinstance(f[0], str)
            and
                isinstance(f[1], (str, type, dl.Type))
            for f in fields
          ) :
            new_fields = []
            for fieldname, fieldtype in fields :
                new_fields.append(Interface.Arg(fieldname, map_field_type(fieldtype)))
            #end for
            fields = new_fields
        else :
            bad = True
        #end if
    elif isinstance(fields, dict) :
        fields = list \
          (
            Interface.Arg(fieldname, map_field_type(fieldtype))
            for fieldname, fieldtype in fields.items()
          )
    else :
        bad = True
    #end if
    if bad :
        raise TypeError \
          (
                "%s defs must be sequence of Interface.Arg or (name, type)"
                " pairs or dict of name→type mappings"
            %
                what
          )
    #end if
    return fields
#end get_field_defs

def structtype \
  (*,
    name,
    fields,
  ) :
    "creates a class that is a trivial wrapper around a Varlink struct type." \
    " The constructor takes and validates arguments corresponding to the fields," \
    " and returns them in a Python dict."

    fields = get_field_defs(fields, "field")

    class struct_class :

        def __new__(celf, *args, **kwargs) :
            return _get_args(fields, args, kwargs)
        #end __new__

    #end struct_class

#begin structtype
    struct_class.__name__ = name
    struct_class._type_info = \
        {
            "name" : dl.validate_name(name),
            "kind" : USERTYPEKIND.STRUCT,
            "fields" : fields,
        }
    return struct_class
#end structtype

def enumtype \
  (*,
    name = None,
    instance_name = None,
  ) :
    "Invoke this function as a decorator on a standard-library Python enum type" \
    " (i.e. a subclass of enum.Enum), and it will automatically collect the instances" \
    " into a Varlink enumerated type definition. instance_name is an optional function" \
    " to map each enum instance to its Varlink enum name string; if omitted, the name" \
    " attribute of each enum instance is used."

    if instance_name == None :
        instance_name = lambda e : e.name
    #end if

    def decorate(celf) :
        if not issubclass(celf, enum.Enum) :
            raise TypeError("only apply decorator to enumerated types.")
        #end if
        if name != None :
            type_name = name
        else :
            type_name = celf.__name__
        #end if
        values = list \
          (
            (i, dl.validate_field_name(instance_name(i)))
            for i in celf
          )
        celf._type_info = \
            {
                "name" : dl.validate_name(type_name),
                "kind" : USERTYPEKIND.ENUM,
                "values" : list(v[1] for v in values),
                "map_values" : dict(values),
            }
        return celf
    #end decorate

#begin enumtype
    return decorate
#end enumtype

def method \
  (*,
    name,
    inargs,
    outargs,
    inargs_keyword = None,
    inargs_keys = None,
    inargs_attrs = None,
    connection_keyword = None,
    message_keyword = None,
    oneway_keyword = None,
    want_more = False,
    more_keyword = None,
  ) :

    inargs = get_field_defs(inargs, "inargs")
    outargs = get_field_defs(outargs, "outargs")

    def decorate(func) :
        if not callable(func) :
            raise TypeError("only apply decorator to callables.")
        #end if
        if name != None :
            func_name = name
        else :
            func_name = func.__name__
        #end if
        if inargs_keys != None and len(inargs_keys) != len(inargs) :
            raise ValueError("inargs_keys must have one entry per inarg")
        #end if
        func._method_info = \
            {
                "name" : dl.validate_name(func_name),
                "inargs" : inargs,
                "outargs" : outargs,
                "inargs_keyword" : inargs_keyword,
                "inargs_keys" : inargs_keys,
                "connection_keyword" : connection_keyword,
                "message_keyword" : message_keyword,
                "oneway_keyword" : oneway_keyword,
                "want_more" : want_more,
                "more_keyword" : more_keyword,
            }
        if inargs_attrs != None :
            func._method_info["inargs_constructor"] = \
                def_attr_class("%s_args" % func_name, inargs_attrs)
        #end if
        return func
    #end decorate

#begin method
    return decorate
#end method

def def_unimpl_method \
  (
    name,
    inargs,
    outargs,
  ) :
    "convenience routine for defining an unimplemented-method stub function." \
    "Instead of\n" \
    "\n" \
    "    @method(«args»)\n" \
    "    def stubfunc() :\n" \
    "        raise ServiceErrorReturn(\"MethodNotImplemented\", {\"method\" : «name»}\n" \
    "    #end stubfunc\n" \
    "\n" \
    "you can do\n" \
    "\n" \
    "    stubfunc = def_unimpl_method(«args»)\n" \
    "\n" \
    "where the only items permitted in «args» are the name, inargs and outargs." \
    " Note that the name arg is required."

    def stub(self) :
        "This is just a stub, standing in for an unimplemented method" \
        " definition in a proxy interface class. Calling it will send" \
        " a MethodNotImplemented error return to the caller."
        raise \
            ServiceErrorReturn("MethodNotImplemented", {"method" : name})
    #end stub

#begin def_unimpl_method
    if name == None :
        raise KeyError("name arg is mandatory")
    #end if
    stub.__name__ = name
    return \
        method(name = name, inargs = inargs, outargs = outargs)(stub)
#end def_unimpl_method

def error(name, inargs) :
    name = dl.validate_name(name)
    inargs = get_field_defs(inargs, "inargs")

    def errorfunc(self, *args, **kwargs) :
        celf = type(self)
        felf = celf._interface_errors[name]["action"]
        raise self._interface_error_return \
          (
            celf._interface_name + "." + name,
            _get_args(felf._error_info["inargs"], args, kwargs)
          )
    #end errorfunc

    errorfunc.__name__ = name
    errorfunc.__doc__ = "raises the “%s” exception" % name
    errorfunc._error_info = \
        {
            "name" : name,
            "inargs" : inargs,
        }

    return errorfunc
#end error

#+
# Introspection
#-

def introspect(interface) :
    "returns a darlink.Interface object that describes the specified" \
    " @interface() class."
    if is_interface(interface) :
        intf = interface
    elif is_interface_instance(interface) :
        intf = type(interface)
    else :
        raise TypeError("interface must be an @interface()-type class")
    #end if

    types = []
    for t in intf._interface_types.values() :
        typeinfo = t._type_info
        if typeinfo["kind"] == USERTYPEKIND.STRUCT :
            vtype = Interface.StructType \
              (
                name = typeinfo["name"],
                fields = typeinfo["fields"]
              )
        elif typeinfo["kind"] == USERTYPEKIND.ENUM :
            vtype = Interface.EnumType \
              (
                name = typeinfo["name"],
                values = typeinfo["values"]
              )
        else :
            raise RuntimeError("unrecognized type kind %s" % repr(thistype["kind"]))
        #end if
        types.append(vtype)
    #end for
    methods = []
    for meth in intf._interface_methods.values() :
        methinfo = meth["action"]._method_info
        method = Interface.Method \
          (
            name = methinfo["name"],
            inargs = methinfo["inargs"],
            outargs = methinfo["outargs"],
            more = methinfo["want_more"],
          )
        methods.append(method)
    #end for
    errors = []
    for err in intf._interface_errors.values() :
        errinfo = err["action"]._error_info
        error = Interface.Error \
          (
            name = errinfo["name"],
            inargs = errinfo["inargs"]
          )
        errors.append(error)
    #end for
    return \
        Interface \
          (
            name = intf._interface_name,
            types = types,
            methods = methods,
            errors = errors
          )
#end introspect

def _get_args(methargs, args, kwargs) :
    # given positional and keyword args, validates them according
    # to methargs and returns a dict of the combined args.
    # fixme: should validate types against methargs, but returning
    # TypeError or ValueError instead of ServiceErrorReturn to
    # indicate caller bug rather than client bug

    def map_val(val) :
        if isinstance(val, enum.Enum) :
            valtype = type(val)
            if hasattr(valtype, "_type_info") :
                type_info = valtype._type_info
                if type_info["kind"] == USERTYPEKIND.ENUM :
                    val = type_info["map_values"][val]
                #end if
            #end if
        #end if
        return val
    #end map_val

#begin _get_args
    if len(args) > len(methargs) :
        raise ValueError("too many args")
    #end if
    combined_args = dict((a.name, None) for a in methargs)
    if len(args) != 0 :
        for i, arg in enumerate(methargs[:len(args)]) :
            arg = args[i]
            if arg != None :
                combined_args[arg.name] = map_val(arg)
            #end if
        #end for
    #end if
    duplicated_args = set()
    spurious_args = set()
    for name, value in kwargs.items() :
        if name in combined_args :
            if value != None :
                if combined_args[name] == None :
                    combined_args[name] = map_val(value)
                else :
                    duplicated_args.add(name)
                #end if
            #end if
        else :
            spurious_args.add(name)
        #end if
    #end for
    if len(spurious_args) != 0 :
        raise KeyError("no such arg(s): %s" % ", ".join(sorted(spurious_args)))
    #end if
    if len(duplicated_args) != 0 :
        raise ValueError("duplicated value for arg(s): %s" % ", ".join(sorted(duplicated_args)))
    #end if
    missing_args = set \
      (
        a.name
        for a in methargs
        if not isinstance(a.type, dl.NullableType) and combined_args[a.name] == None
      )
    if len(missing_args) != 0 :
        raise ValueError("missing value for arg(s): %s" % ", ".join(sorted(missing_args)))
    #end if
    return combined_args
#end _get_args

class ProxyInterface :
    "abstract base class for identifying proxy interface classes."

    __slots__ = ()

#end ProxyInterface

def def_proxy_interface(name, introspected) :
    "given a darlink.Interface object, creates a proxy class that can be" \
    " instantiated by a client to send asynchronous method-call messages" \
    " to a server. The resulting class can be instantiated by\n" \
    "\n" \
    "    instance = proxy_class(«connection»)\n" \
    "\n" \
    " where «connection» is a ConnectionAsync object to use for sending and" \
    " receiving the messages."

    if not isinstance(introspected, Interface) :
        raise TypeError("introspected must be an Interface")
    #end if

    def write_type(t) :
        out = io.StringIO()
        if t.name != None :
            out.write(t.name)
        else :
            if isinstance(t, dl.NullableType) :
                out.write("?")
                out.write(write_type(t.targtype))
            elif isinstance(t, dl.ArrayType) :
                out.write("[] ")
                out.write(write_type(t.elttype))
            elif isinstance(t, dl.DictType) :
                out.write("[string] ")
                out.write(write_type(t.valuetype))
            elif isinstance(t, Interface.StructType) :
                out.write("(")
                out.write(", ".join("%s : %s" % (f.name, write_type(f.type)) for f in t.fields))
                out.write(")")
            elif isinstance(t, Interface.EnumType) :
                out.write("(")
                out.write(", ".join(v for v in t.values))
                out.write(")")
            else :
                raise TypeError("not a recognized Type: %s" % repr(t))
            #end if
        #end if
        return out.getvalue()
    #end write_type

    class proxy(ProxyInterface) :
        # class that will be constructed, to be instantiated for a given connection.

        # class field _iface_name contains interface name.
        __slots__ = ("_conn", "_timeout")

        def __init__(self, connection, *, timeout = None) :
            self._conn = connection.conn # only need the darlink object right now
            self._timeout = timeout
        #end __init__

        # rest filled in dynamically below.

    #end proxy

    def def_type(typedef) :
        # constructs a Python wrapper for a Varlink struct or enum type.
        if isinstance(typedef, Interface.StructType) :

            class vtype :

                _fields = typedef.fields
                _field_names = set(f.name for f in _fields)

                def __new__(celf, **kwargs) :
                    return \
                        Interface.Arg.validate_arglist \
                          (
                            "fields of %s" % typedef.name,
                            celf._fields, celf._field_names,
                            kwargs,
                            dl.InvalidParamError
                          )
                #end __new__

            #end vtype

            vtype.__name__ = typedef.name
        elif isinstance(typedef, Interface.EnumType) :
            vtype = enum.EnumType.__call__ \
              (
                cls = enum.Enum,
                value = typedef.name,
                module = introspected.name,
                names = tuple((v, v) for v in typedef.values),
                type = str
              )
        else :
            raise TypeError("not a recognized derived type: %s" % repr(typedef))
        #end if
        setattr(proxy, typedef.name, vtype)
    #end def_type

    def def_method(methdef) :
        # constructs a method-call method.

        if methdef.more :

            async def call_method(self, *args, **kwargs) :
                msg = Message.create_method \
                  (
                    method = introspected.name + "." + methdef.name,
                    parameters = _get_args(methdef.inargs, args, kwargs),
                    validate = methdef,
                    oneway = False,
                    more = True
                  )
                async for reply in self._conn.transact(msg, timeout = self._timeout) :
                    if reply.is_error :
                        raise reply.to_error()
                    else :
                        result = reply.parameters
                        methdef.validate_outargs(result, dl.InvalidParamErrorReturn)
                        yield result
                    #end if
                #end for
            #end call_method

        else :

            async def call_method(self, *args, **kwargs) :
                msg = Message.create_method \
                  (
                    method = introspected.name + "." + methdef.name,
                    parameters = _get_args(methdef.inargs, args, kwargs),
                    validate = methdef,
                    oneway = methdef.oneway,
                    more = False
                  )
                if methdef.oneway :
                    self._conn.queue_send(msg)
                    result = None
                else :
                    reply = await self._conn.transact1(msg, timeout = self._timeout)
                    if reply.is_error :
                        raise reply.to_error()
                    #end if
                    result = reply.parameters
                    methdef.validate_outargs(result, dl.InvalidParamErrorReturn)
                #end if
                return result
            #end call_method

        #end if

    #begin def_method
        call_method.__name__ = methdef.name
        call_method.__doc__ = \
            (
                "method, %(inargs)s, %(outargs)s"
            %
                {
                    "inargs" :
                        (
                            lambda : "no inargs",
                            lambda :
                                    "inargs (%s)"
                                %
                                    ", ".join
                                      (
                                        "%s : %s" % (arg.name, write_type(arg.type))
                                        for arg in methdef.inargs
                                      ),
                        )[len(methdef.inargs) != 0](),
                    "outargs" :
                        (
                            lambda : "no outargs",
                            lambda :
                                    "outargs (%s)"
                                %
                                    ", ".join
                                      (
                                        "%s : %s" % (arg.name, write_type(arg.type))
                                        for arg in methdef.outargs
                                      ),
                        )[len(methdef.outargs) != 0](),
                }
            )
        setattr(proxy, methdef.name, call_method)
    #end def_method

#begin def_proxy_interface
    if name != None :
        class_name = name
    else :
        class_name = introspected.name.replace(".", "_").replace("-", "_")
    #end if
    proxy.__name__ = class_name
    proxy._iface_name = introspected.name
    proxy.__doc__ = \
        (
            "for sending method calls on the %(intf)s interface."
        %
            {
                "intf" : introspected.name,
            }
        )
    for vtype in introspected.types :
        def_type(vtype)
    #end for
    for method in introspected.methods :
        def_method(method)
    #end for
    return proxy
#end def_proxy_interface

#+
# Predefined interfaces
#-

@interface("org.varlink.service")
class IntrospectionHandler :
    "Register this to obtain automatic introspection of all registered interfaces."

    __slots__ = ("vendor", "product", "version", "url")

    def __init__(self, vendor, product, version, url) :
        if not all(isinstance(s, str) for s in (vendor, product, version, url)) :
            raise ValueError("missing/invalid args")
        #end if
        self.vendor = vendor
        self.product = product
        self.version = version
        self.url = url
    #end __init__

    @method \
      (
        name = "GetInfo",
        inargs = [],
        outargs =
            [
                ("vendor", "string"),
                ("product", "string"),
                ("version", "string"),
                ("url", "string"),
                ("interfaces", dl.ArrayType(None, "string")),
            ],
        connection_keyword = "conn"
      )
    def get_interface_info(self, *, conn) :
        interfaces = []
        for intf in conn.dispatch.values() :
            interfaces.append(introspect(intf).to_string())
        #end for
        return \
            {
                "vendor" : self.vendor,
                "product" : self.product,
                "version" : self.version,
                "url" : self.url,
                "interfaces" : interfaces,
            }
    #end get_interface_info

    @method \
      (
        name = "GetInterfaceDescription",
        inargs =
            [
                ("interface", "string"),
            ],
        inargs_keys = ["interface"],
        outargs =
            [
                ("description", "string"),
            ],
        connection_keyword = "conn"
      )
    def get_interface_description(self, *, conn, interface) :
        intf = conn.dispatch.get(interface)
        if intf == None :
            raise ServiceErrorReturn("InterfaceNotFound", {"interface" : interface})
        #end if
        return {"description" : introspect(intf).to_string()}
    #end get_interface_description

#end IntrospectionHandler

#+
# Cleanup
#-

def _atexit() :
    # disable all __del__ methods at process termination to avoid unpredictable behaviour
    for cls in ConnectionAsync, :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
