#+
# Setuptools script to install Darlink. Make sure setuptools
# <https://setuptools.pypa.io/en/latest/index.html> is installed.
# Invoke from the command line in this directory as follows:
#
#     python3 setup.py build
#     sudo python3 setup.py install
#
# Written by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
#-

import sys
import setuptools
from setuptools.command.build_py import \
    build_py as std_build_py

class my_build_py(std_build_py) :
    "customization of build to perform additional validation."

    def run(self) :
        try :
            exec \
              (
                "async def dummy() :\n"
                "    pass\n"
                "#end dummy\n"
                "\n"
                "import asyncio\n"
                "\n"
                "asyncio.run\n"
              )
        except (SyntaxError, ImportError, NameError) :
            sys.stderr.write("This module requires Python 3.8 or later.\n")
              # actually 3.7 is probably fine
            sys.exit(-1)
        #end try
        super().run()
    #end run

#end my_build_py

setuptools.setup \
  (
    name = "Varlink",
    version = "0.7",
    description = "implementation of Varlink protocol, for Python 3.8 or later",
    long_description = "implementation of Varlink protocol, for Python 3.8 or later",
    author = "Lawrence D'Oliveiro",
    author_email = "ldo@geek-central.gen.nz",
    url = "https://gitlab.com/ldo/darlink",
    license = "LGPL v2.1+",
    py_modules = ["darlink", "larvink"],
    cmdclass =
        {
            "build_py" : my_build_py,
        },
  )
